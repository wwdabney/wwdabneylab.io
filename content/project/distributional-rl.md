+++
title = "Distributional RL"
date = 2018-02-17T18:39:03Z
draft = false

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = []

# Project summary to display on homepage.
summary = ""

# Optional image to display on homepage.
image_preview = ""

# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Does the project detail page use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

<center>
<div style="background-image:url(../../img/distrl_header.png);width:0px;height:0px;outline:none;padding:120px 120px;background-repeat:no-repeat;"></div>
</center>