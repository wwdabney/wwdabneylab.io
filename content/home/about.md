+++
# About/Biography widget.
widget = "about"
active = true
date = "2016-04-20T00:00:00"

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
    "Policy improvement in distributional RL",
    "Reinforcement learning from sparse rewards",
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "PhD in Computer Science"
  institution = "University of Massachusetts, Amherst"
  year = 2014

[[education.courses]]
  course = "BSc in Computer Science"
  institution = "University of Oklahoma"
  year = 2007
 
+++

# Biography

Currently, I am a staff research scientist at <a href="https://deepmind.com/">DeepMind</a>, 
where I study reinforcement learning with forays into other topics in machine learning. 

My research agenda focuses on finding the critical path to human-level AI.
I believe we are in fact only a handful of great papers away from the most significant breakthrough in human history.
With the help of my collaborators, I hope to move us closer; one paper, experiment, or conversation at a time.
