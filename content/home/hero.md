+++
# Hero widget.
widget = "hero"
active = true
date = 2017-10-15

#title = "Science"

# Order that this section will appear in.
weight = 10 #3

# Overlay a color or image (optional).
#   Deactivate an option by commenting out the line, prefixing it with `#`.
[header]
  overlay_color = "#666"  # An HTML color value.
  overlay_img = "headers/bubbles-wide.jpg"  # Image path relative to your `static/img/` folder.
  overlay_filter = 0.5  # Darken the image. Value in range 0-1.

+++

<p class="hero-lead">Nothing in life is to be feared, it is only to be understood.<br>
Now is the time to understand more, so that we may fear less.<br>
<div class="hero-lead" style="line-height: 0; text-align: right;">&#8212; Marie Curie</div></p>


